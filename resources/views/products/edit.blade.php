@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <i class="fa fa-fw fa-edit"></i>
                Atualizar Produto {{ $product->name }}
            </h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <a role="button" class="btn btn-default" href="{{ route('produtos.index') }}">
                    <i class="fa fa-fw fa-arrow-left"></i>
                    Voltar
                </a>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('produtos.update', ['id' => $product->id]) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nome do Produto</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Descrição</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="description" name="description">{{ $product->description }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">
                            Atualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop