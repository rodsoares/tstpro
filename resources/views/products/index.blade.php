@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <i class="fa fa-fw fa-shopping-basket"></i>
                Produtos
            </h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <a role="button" class="btn btn-primary" href="{{ route('produtos.create') }}">
                    <i class="fa fa-fw fa-plus-circle"></i>
                    Novo Produto
                </a>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body">
            <table class="table table-sm table-condensed table-bordered" id="products-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach( $products as $product )
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td class="text-right">
                            <div class="btn-group btn-group-xs" role="group">
                                <a role="button" class="btn btn-default" title="editar" href="{{ route('produtos.edit', ['id' => $product->id]) }}">
                                    <i class="fa fa-fw fa-edit"></i>
                                </a>
                            </div>

                            <div class="btn-group btn-group-xs" role="group">
                                <button type="button" class="btn btn-danger" title="deletar" onclick="deleteProduct({{$product->id}})">
                                    <i class="fa fa-fw fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <form id="delete-form" method="POST">{{ csrf_field() }}{{ method_field('DELETE') }}</form>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
    <script>
        function deleteProduct(id) {
            swal({
                    title: "Você tem certeza dessa ação?",
                    text: "O arquivo será permanentemente apagado da base de dados",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        var form = $('#delete-form');
                        form.attr('action', '/produtos/'+id);
                        console.log( form.attr('action') );
                        form.submit();
                    } else {
                        swal("Cancelado", "Produto não será apagado da base de dados", "error");
                    }
                });
        }


        $(document).ready(function(){
            $('#products-table').dataTable();
        });
    </script>
@stop