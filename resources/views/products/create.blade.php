@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <i class="fa fa-fw fa-plus-circle"></i>
                Novo Produto
            </h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <a role="button" class="btn btn-default" href="{{ route('produtos.index') }}">
                    <i class="fa fa-fw fa-arrow-left"></i>
                    Voltar
                </a>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body">
            <form id="add-product-form" class="form-horizontal" action="{{ route('produtos.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nome do Produto</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Descrição</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="description" name="description" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button
                                id="btn-process-form"
                                type="button"
                                class="btn btn-success"
                                data-loading-text="Processando requisição..."
                        >
                            Adicionar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        $('#btn-process-form').click(function (e) {
            $(this).button('loading');
            $('#add-product-form').submit();
        });
    </script>
@stop