<?php

namespace tstprod\Http\Controllers;

use tstprod\Http\Requests\Product\StoreRequest;
use tstprod\Http\Requests\Product\UpdateRequest;

use tstprod\Models\Products;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::paginate(15);
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $product = Products::create( $request->all() );
            return redirect()->route('produtos.index')->with([
                'status' => 'success',
                'msg'    => "Produto $product->name adicionado a base de dados"
            ]);
        }catch(\Exception $e) {
            return redirect()->route('produtos.index')->with([
                'status' => 'danger',
                'msg'    => "Ocorreu um erro no processamento. Tente novamente em alguns instantes."
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $product = Products::findOrFail( $id );
            return view('products.edit', compact('product'));
        }catch(\Exception $e) {
            return redirect()->route('products.index')->with([
                'status' => 'danger',
                'msg'    => "Produto inexistente"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateRequest $request, $id)
    {
        try {
            $product = Products::findOrFail( $id );
            $product->fill( $request->all() );
            $product->save();
            return redirect()->route('produtos.index')->with([
                'status' => 'warning',
                'msg'    => "Produto $product->name atualizado"
            ]);
        }catch(\Exception $e) {
            return redirect()->route('produtos.index')->with([
                'status' => 'danger',
                'msg'    => "Ocorreu um erro no processamento. Tente novamente em alguns instantes."
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Products::findOrFail( $id );
            $name    = $product->name;
            $product->delete();
            return redirect()->route('produtos.index')->with([
                'status' => 'warning',
                'msg'    => "Produto $name deletado"
            ]);
        }catch(\Exception $e) {
            return redirect()->route('produtos.index')->with([
                'status' => 'danger',
                'msg'    => "Ocorreu um erro no processamento. Tente novamente em alguns instantes."
            ]);
        }
    }
}
